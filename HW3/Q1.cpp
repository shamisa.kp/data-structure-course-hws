#include <iostream>
#include <algorithm>

using namespace std;

int mSum{0};
int* melon;
long long int ***table;

long long int countWays(int l, int maxSIndex, int difference) {

    if(l == 0) {
        return difference == mSum/2 ? 1 : 0;
    }

    if(difference < 0 || difference > mSum) return 0;

    if(table[difference][l][maxSIndex] != -1) return table[difference][l][maxSIndex];

    int tD{difference - melon[l - 1]};
    int m{0};

    if(melon[maxSIndex] > melon[l - 1]){
        m = maxSIndex;
    } else m = l - 1;


    long long int dp1{countWays(l - 1, m, tD)};
    long long int dp2{countWays(l - 1, maxSIndex, difference)};
    long long int dp3{0};

    tD = difference + melon[l - 1];

    if(melon[maxSIndex] <= melon[l - 1]) {
        dp3 = countWays(l - 1, maxSIndex, tD);
    }

    return table[difference][l][maxSIndex] = dp1 + dp2 + dp3;

}


int main() {
    
    int n;
    cin >> n;

    melon = new int[n];

    for (int i = 0; i < n; ++i) {
        cin >> melon[i];
        mSum += melon[i];
    }


    sort(melon, melon + n);

    table = new long long int**[mSum + 2];
    for (int j = 0; j < mSum + 2; ++j) {
        table[j] = new long long int*[n + 1];
        for (int i = 0; i < n + 1; ++i) {
            table[j][i] = new long long int[n + 1];
            for (int k = 0; k < n + 1; ++k) {
                table[j][i][k] = -1;
            }
        }

    }

    cout << countWays(n, 0, mSum / 2) - 1 << endl;
    return 0;
}