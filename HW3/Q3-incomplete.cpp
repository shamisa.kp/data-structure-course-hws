#include <iostream>
#include <climits>
#include <cstdio>
#include <climits>
#include <limits>

using namespace std;


int vertex;
int* parent;
int* myDist;
int ** nCity;

int minDistance(const bool visited[]) {

    int minI{0};
    int mininum =  numeric_limits<int>::max();


    for (int u{0}; u < vertex; ++u) {
        if (!visited[u] && myDist[u] <= mininum) {
            mininum = myDist[u];
            minI = u;
        }

    }
    return minI;
}

void dijkstra(int** g, int s) {

    bool visited[vertex];

    for (int i = 0; i < vertex; ++i) {
        myDist[i] = INT_MAX;
        visited[i] = false;
    }

    myDist[s] = 0;

    for (int j{0}; j < vertex - 1; ++j) {
        int u{minDistance(visited)};
        visited[u] = true;

        for(int i = 0; i < vertex; ++i) {
            if (!visited[i] && g[u][i] && myDist[u] != INT_MAX && g[u][i] != INT32_MAX) {
                if (myDist[u] + g[u][i] < myDist[i]) {
                    myDist[i] = myDist[u] + g[u][i];
                    parent[i] = u;
                    nCity[s][i] = nCity[s][u] + nCity[u][i];
                } else if (myDist[u] + g[u][i] == myDist[i]) {
                    if (nCity[s][u] + nCity[u][i] < nCity[s][i]) {
                        nCity[s][i] = nCity[s][u] + nCity[u][i];
                        parent[i] = u;
                    }
                }
            }
        }
    }
}

int main() {
    vertex = 1000;
    auto graph = new int *[1000];
    nCity = new int *[1000];
    for (int i = 0; i < 1000; i++) {
        graph[i] = new int[1000];
        nCity[i] = new int[1000];
        for (int j = 0; j < 1000; j++) {
            graph[i][j] = nCity[i][j] = INT32_MAX;
        }
        graph[i][i] = nCity[i][i] = 0;
    }

    int x, y, n;

    cin >> x >> y >> n;
    x--;
    y--;

    auto *m = new int[n];
    auto *w = new int[n];

    for (int i = 0; i < n; i++) {

        cin >> w[i] >> m[i];

        auto *path = new int[m[i]];

        for (int j = 0; j < m[i]; j++) {

            cin >> path[j];

            path[j]--;
        }

        for (int j = 0; j < m[i]; j++) {
            for (int k = j + 1; k < m[i]; k++) {
                if (w[i] < graph[path[j]][path[k]]) {
                    graph[path[j]][path[k]] = w[i];
                    nCity[path[j]][path[k]] = k - j;
                } else if (w[i] == graph[path[j]][path[k]]) {
                    nCity[path[j]][path[k]] = min(k - j, nCity[path[j]][path[k]]);
                }
            }
        }
        delete[] path;
    }

    myDist = new int[1000];
    parent = new int[1000];


    for (int i = 0; i < 1000; i++)
        parent[i] = -1;


    dijkstra(graph, x);


    if (myDist[y] < INT_MAX) {
        cout << myDist[y] << " " << nCity[x][y] << endl;
    } else {
        cout << -1 << " " << -1 << endl;
    }


    return 0;
}