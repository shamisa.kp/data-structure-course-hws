#include <iostream>
#include <string>
#include <vector>

using namespace std;

void findPalindrom(string& s) {
    unsigned long n{s.size()};

    int** table{new int*[n]};
    int** wayOfPD{new int*[n]};

    for (int i = 0; i < n; ++i) {

        table[i] = new int[n];
        wayOfPD[i] = new int[n];

        for (int j = 0; j < n; ++j) {
            table[i][j] = 0;
            wayOfPD[i][j] = 0;
        }
    }

    int g, l, h;

    for (g = 1; g < n; ++g) {
        for (l = 0, h = g; h < n; ++l, ++h) {
            if(s[l] == s[h]) {
                table[l][h] = table[l + 1][h - 1];
                wayOfPD[l][h] = 1;
            }
            else if(table[l][h - 1] == table[l + 1][h]){
                if(int(s[h]) < int(s[l])) {
                    table[l][h] = table[l][h - 1] + 1;
                    wayOfPD[l][h] = 3;
                }
                else {
                    table[l][h] = table[l + 1][h] + 1;
                    wayOfPD[l][h] = 2;
                }
            }
            else if(table[l + 1][h] < table[l][h - 1]){
                table[l][h] = table[l + 1][h] + 1;
                wayOfPD[l][h] = 2;
            }
            else{
                table[l][h] = table[l][h - 1] + 1;
                wayOfPD[l][h] = 3;
            }
        }
    }

    int change{table[0][n - 1]};

    if(change == 0){
        cout << s << endl;
    }
    else {

        int l_temp, h_temp;
        l_temp = 0;
        h_temp = static_cast<int>(n) - 1;
        int i{static_cast<int>(n) + change - 1};
        int j{0};
        string answer;
        for(int k = 0; k <= i; k++) answer += '0';

        int outOfTable{wayOfPD[0][n - 1]};

        while(i >= j){
            if(outOfTable == 1){
                answer[i] = s[l_temp];
                answer[j] = s[h_temp];
                outOfTable = wayOfPD[l_temp + 1][h_temp - 1];
                h_temp--;
                l_temp++;
            }
            else if(outOfTable == 2){
                answer[j] = s[l_temp];
                answer[i] = s[l_temp];
                outOfTable = wayOfPD[l_temp + 1][h_temp];
                l_temp++;
            }
            else if(outOfTable == 3){
                answer[j] = s[h_temp];
                answer[i] = s[h_temp];
                outOfTable = wayOfPD[l_temp][h_temp - 1];
                h_temp--;
            }
            else{
                answer[i] = s[h_temp];
            }
            j++;
            i--;
        }
        cout << answer << endl;
    }
}

int main() {

    string input;

    cin >> input;
   findPalindrom(input);


    return 0;
}