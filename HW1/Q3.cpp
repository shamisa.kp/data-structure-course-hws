#include <iostream>
#include <string>

using namespace std;

int main() {
    long int pL, maxL{0};
    string p;

    cin >> pL;
    cin >> p;

    for(long int i{0}; i < pL;) {
        long int l{1};
        long int counter{1};
        for (long int j{i + 1}; j < pL; j++) {
            if (p[j] == ')') {
                counter--;

                if (l > maxL) {
                    maxL = l;

                }
                if (counter == 0) {
                    i = j + 1;
                    break;
                }
            }
            if (p[j] == '(') {
                l += 2;
                counter++;
            }
        }
    }

    cout << maxL;

    return 0;
}