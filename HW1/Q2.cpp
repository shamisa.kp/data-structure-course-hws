#include <iostream>
#include <vector>
#include <string>
#include <list>

using namespace std;

int main() {
    long int qN{0};
    long int garbage{0};
    vector<string> q;

    cin >> qN;

    list<long int> queue;

    cin.ignore();

    for(int i{0}; i < qN; i++) {
        string tempQ;
        getline(cin, tempQ);
        q.push_back(tempQ);
    }

    for (int j{0}; j < qN ; j++) {

        if(q[j].length() == 3) {
            cout << queue.front() << endl;
            garbage = queue.front();
            queue.pop_front();

        } else if(q[j].length() == 4) {

            if(q[j - 1].length() == 3) {
                queue.push_front(garbage);

            } else
                queue.pop_back();

        } else {
            garbage = stol(q[j].substr(8, q[j].length()));
            queue.push_back(garbage);
        }
    }

    return 0;
}