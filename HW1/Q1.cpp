#include <iostream>
#include <stack>
#include <string>
#include <limits>
#include <vector>
#include <algorithm>

using namespace std;

int main() {
    long int q_n{0}; //number of questions
    vector<string> q;
    cin >> q_n;

    stack <long int> s; //defining a stack
    stack <long int> min_value; //for min_value values

    cin.ignore(); //solve the problem with getline function

    /* reading the questions */
    for(int j{0}; j < q_n; j++) {
        string temp_q;
        getline(cin, temp_q);
        q.push_back(temp_q);
    }

    min_value.push(numeric_limits<long int>::max());

    /* checking the instructions */
    for(int i{0}; i < q_n; i++) {
        if (q[i].length() >= 4) {
            if (q[i].substr(0, 4) == "push") {
                long int value_push = stol(q[i].substr(5, q[i].length()));
                if (value_push <= min_value.top()) {
                    min_value.push(value_push);
                    s.push(value_push);
                }
                else
                    s.push(value_push);

            } else if (q[i] == "spell")
                cout << min_value.top() << endl;

        } else if (q[i] == "pop") {
            if (!s.empty()) {
                if (s.top() == min_value.top()) {
                    s.pop();
                    min_value.pop();
                } else
                    s.pop();
            }
        }
    }

    return 0;
}