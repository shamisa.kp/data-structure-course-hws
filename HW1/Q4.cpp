#include <iostream>
#include <vector>
#include <algorithm>

using namespace std;

bool myfunc (int i, int j) { return (i > j); }

int main() {
    long int n, maxW{0};
    long int tempW{0};
    long int place{0};

    vector<long int> myVec;

    cin >> n >> maxW;

    for(long int j{0}; j < n; j++) {
        cin >> tempW;
        myVec.push_back(tempW);
    }


    sort(myVec.begin(), myVec.end(), myfunc);

    std::vector<long int>::iterator end{myVec.end() - 1};

    for (std::vector<long int>::iterator it {myVec.begin()}; it <= end; it++) {

        if (*it + *end<= maxW) {
            end--;
        }
        place++;
    }

    cout << place;

    return 0;
}

