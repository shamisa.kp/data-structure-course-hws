#include <iostream>
#include <queue>


using namespace std;

bool is_finished(const char* s, const char* t) {
    return s == t;
}
bool is_valid(int i, int j, int n, int m) {
    return i >= 0 && i < n && j >= 0 && j < m;
}

void expand_fire(int i, int j, int n, int m, char** arr) {
    if(is_valid(i - 1, j - 1, n, m))
        arr[i - 1][j - 1] = 'f';
    if(is_valid(i - 1, j, n, m))
        arr[i - 1][j] = 'f';
    if(is_valid(i - 1, j + 1, n, m))
        arr[i - 1][j + 1] = 'f';
    if(is_valid(i, j - 1, n, m))
        arr[i][j - 1] = 'f';
    if(is_valid(i, j + 1, n, m))
        arr[i][j + 1] = 'f';
    if(is_valid(i + 1, j - 1, n, m))
        arr[i + 1][j - 1] = 'f';
    if(is_valid(i + 1, j, n, m))
        arr[i + 1][j] = 'f';
    if(is_valid(i + 1, j + 1, n, m))
        arr[i + 1][j + 1] = 'f';

}

void make_fire_bigger(int n, int m, char** arr) {
    for (int i{0}; i < n; ++i) {
        for (int j{0}; j < m; ++j) {
            if(arr[i][j] == 'f'){
                expand_fire(i, j, n, m, arr);
            }
        }
    }
}

bool mohsen_is_dead(const char* s,const char* t) {
    return *s == *t;
}

class QItem {
public:
    int row;
    int col;
    int dist;
    QItem(int x, int y, int w)
            : row(x), col(y), dist(w)
    {}
};

int minDistance(char **grid, int N, int M)
{
    QItem source(0, 0, 0);

    bool visited[N][M];
    for (int i = 0; i < N; i++) {
        for (int j = 0; j < M; j++)
        {
            if ('-' == grid[i][j]) {
                visited[i][j] = true;
            } else {
                visited[i][j] = false;
            }

            // Finding source
            if (grid[i][j] == 's')
            {
                source.row = i;
                source.col = j;
            }
        }
    }

    queue<QItem> q;
    q.push(source);
    visited[source.row][source.col] = true;
    while (!q.empty()) {
        QItem p = q.front();
        q.pop();

        // Destination found;
        if (grid[p.row][p.col] == 't')
            return p.dist;

        // moving up
        if (p.row - 1 >= 0 &&
            !visited[p.row - 1][p.col]) {
            q.push(QItem(p.row - 1, p.col, p.dist + 1));
            visited[p.row - 1][p.col] = true;
        }

        // moving down
        if (p.row + 1 < N &&
            !visited[p.row + 1][p.col]) {
            q.push(QItem(p.row + 1, p.col, p.dist + 1));
            visited[p.row + 1][p.col] = true;
        }

        // moving left
        if (p.col - 1 >= 0 &&
            !visited[p.row][p.col - 1]) {
            q.push(QItem(p.row, p.col - 1, p.dist + 1));
            visited[p.row][p.col - 1] = true;
        }

        // moving right
        if (p.col + 1 < M &&
            !visited[p.row][p.col + 1]) {
            q.push(QItem(p.row, p.col + 1, p.dist + 1));
            visited[p.row][p.col + 1] = true;
        }
    }
    return -1;
}

int main() {

    int N{0};
    int M{0};
    int k{0};

    char* s{nullptr};
    char* t{nullptr};

    cin >> N >> M >> k;

    char** grid = new char*[N];

    for (int i{0}; i < N; ++i) {
        grid[i] = new char[M];
        char tempchar;
        for (int j{0}; j < M; ++j) {
            cin >> tempchar;
            if(tempchar == 's') {
                grid[i][j] = tempchar;
                s = &grid[i][j];
            }
            else if(tempchar == 't') {
                grid[i][j] = tempchar;
                s = &grid[i][j];
            }
        }
    }
    int cnt{0};

//    while(!is_finished(s, t)) {
//
//        cnt++;
//        if(cnt % k == 0){
//            make_fire_bigger(N, M, grid);
//        }
//        if(mohsen_is_dead(s, t)) {
//            cout << "Impossible";
//            break;
//        }
//
//    }
    cout << minDistance(grid, N, M);
    return 0;
}
