#include <iostream>
#include <list>
#include <cstring>
#include <queue>


using namespace std;

//class for graph
class Graph {

    int vertex;
    list<int> *adjacent;

public:
    Graph(int vertex);
    void add_edge(int vertex, int p);
    int longest_path();
    pair<int, int> BFS(int n);

};

Graph::Graph(int vertex) {
    this->vertex = vertex;
    adjacent = new list<int>[vertex];
}

void Graph::add_edge(int vertex, int p) {
    adjacent[vertex].push_back(p);
    adjacent[p].push_back(vertex);
}

pair<int, int> Graph::BFS(int n) {

    int distance[vertex];
    memset(distance, -1, sizeof(distance));

    queue<int> Q;
    Q.push(n);

    distance[n] = 0;

    while (!Q.empty()) {
        int w{Q.front()};
        Q.pop();


        for (auto itr = adjacent[w].begin(); itr != adjacent[w].end(); itr++) {
            int p{*itr};
            if (distance[p] == -1) {
                Q.push(p);
                distance[p] = distance[w] + 1;
            }
        }
    }

    int max{0};
    int node_index{0};
    for (int i{0}; i < vertex; ++i) {
        if (max < distance[i]) {
            max = distance[i];
            node_index = i;
        }
    }

    return make_pair(node_index, max);
}

int Graph::longest_path() {

    pair<int, int> u1{BFS(0)};

    pair<int, int> u2{BFS(u1.first)};

    return u2.second;
}


int main() {

    int n{0};

    cin >> n;
    Graph temp_g{n};
    int a{0};
    int b{0};

    for (int i{0}; i < n - 1; ++i) {
        cin >> a >> b;
        temp_g.add_edge(a - 1, b - 1);
    }

    cout << temp_g.longest_path() << endl;

    return 0;
}