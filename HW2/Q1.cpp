#include <iostream>
#include <cstdlib>


using namespace std;

struct Node {

    int key; //value of node

    int right_counter;

    Node* left_child;
    Node* right_child;

};

Node* insert(Node *root, int key) {

    auto* node = new Node;
    node->key = key;
    node->right_counter = 0;
    node->right_child = nullptr;
    node->left_child = nullptr;

    Node* parent = root;
    Node* search_node = root;

    //find the place for new node
    while (search_node) {
        parent = search_node;

        if (node->key >= search_node->key) {

            search_node->right_counter++;
            search_node = search_node->right_child;
        } else {
            search_node = search_node->left_child;
        }
    }

    //when there is no root
    if (!root) {
        root = node;
    } else if (node->key >= parent->key) {
        parent->right_child = node;
    } else {
        parent->left_child = node;
    }

    return root;
}

int k_largest_element(Node *root, int k) {

    if (root) {
        Node* search_node = root;

        while (search_node) {
            if ((search_node->right_counter + 1) == k) {
                return search_node->key;
            } else if (k > search_node->right_counter) {
                k = k - (search_node->right_counter + 1);
                search_node = search_node->left_child;
            } else {
                search_node = search_node->right_child;
            }
        }
    }
    return -1;
}


int main() {

    int n{0}; //number of queries
    int q{0};


    cin >> n;

    Node* root{nullptr};


    int counter{0};
    //loop for queries
    for (int i{0}; i < n ; ++i) {

        cin >> q;

        if (q == 1) {
            counter++;
            int score{0};
            cin >> score;
            root = insert(root, score);
        } else if (counter >= 3) {
            int k{static_cast<int>(counter/3)};
            cout << k_largest_element(root, k) << endl;
        } else {
            cout << "No reviews yet" << endl;
        }
    }

    return 0;
}

