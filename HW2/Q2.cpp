#include <iostream>
#include <queue>



using namespace std;
typedef long long int LLI;

int main() {

    LLI n{0};
    cin >> n; //number of gps

    priority_queue<LLI, vector<LLI>, greater<LLI>> q;

    LLI group_friends{0};

    for (LLI i{0}; i < n ; ++i) {
        cin >> group_friends;
        q.push(group_friends);
    }


    LLI total_expense{0};
    while (q.size() != 1) {
        LLI min{q.top()};
        q.pop();
        min += q.top();
        q.pop();
        q.push(min);
        total_expense += min;
        if(q.size() == 1)
            total_expense += min;
    }
    
    cout << total_expense << endl;

    return 0;
}